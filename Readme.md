# Installé sur l'hébergement O2Switch
Le projet a été généré à partir de [PUG](https://pugjs.org/api/getting-started.html), et ne comporte que du HTML et du SCSS.
Sur la page d'accueil, entrer un mail valide et un mot de passe (fictifs) contenant au moins 8 caractères, dont au moins une majuscule, une minuscule, un nombre et un caractère spécial : le formulaire contient un contrôle par regex sur chaque champ.

Le résultat du build est disponible à cette adresse :

http://mathieugodard.fr/exercices/html-css/Digital-Campus-Soutenance/

# Build du projet

1/ Installation des paquets
npm i

2/ Suppression du dossier de distribution, copie des feuilles de style et des immage, puis lancement du html-plug avec watcher
npm run start

3/ Lancement du paquet reload pour mettre à jour le site dans le navigateur
npm run reload


# Command line instructions

You can also upload existing files from your computer using the instructions below.

## Git global setup
```bash
git config --global user.name "Mathieu"
git config --global user.email "mathieu.godard@zaclys.net"
```
## Create a new repository
```bash
git clone git@framagit.org:mathieugodard/digital-campus-projet-html-scss-pug.git
cd digital-campus-projet-html-scss-pug
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```
## Push an existing folder
```bash
cd existing_folder
git init --initial-branch=main
git remote add origin git@framagit.org:mathieugodard/digital-campus-projet-html-scss-pug.git
git add .
git commit -m "Initial commit"
git push -u origin main
```
## Push an existing Git repository
```bash
cd existing_repo
git remote rename origin old-origin
git remote add origin git@framagit.org:mathieugodard/digital-campus-projet-html-scss-pug.git
git push -u origin --all
git push -u origin --tags
```
